// url_test.ts
import {assertEquals} from "https://deno.land/std@0.223.0/assert/mod.ts";

interface EmployeeRecord extends Record<string, any> {
    AÑO: String
    QUINCENA: String
    HASH: number
    NOMBRE: String
    "FECHA INGRESO": String
    "FECHA ANIV.": String
    "Años cumplidos": String
    FOLIO: String
    "INICIAL 2022": number
    "DIAS ANIV": number
    GOZADOS: number
    "Finiquitos / Liq.": String
    DISPONIBLES: number
    "ANIV PAGADOS": number
    "ACUM DIAS PEND DE PAGO DE PV": number
    unnamed1: String
    Estatus: String
    "dias pv": number
    "periodo pago pv": String
    Obs: String
    unnamed2: String
    unnamed3: String
    unnamed4: String
    unnamed5: String
    unnamed6: String
    unnamed7: String
}

interface Employee {
    id: number
    name: String
    records: EmployeeRecord[]
    totalAvailable: number
}

// Deno.test("url test", () => {
const vacText = Deno.readTextFileSync("./test/rsrc/vac_2024.csv")

let vacResult = vacText.split(/\n/g).map(row => row.split(/,/g))

let employees: Employee[] = []

let test = vacResult.slice(1).map((row: string[]) => {
    const records: EmployeeRecord = {
        AÑO: "",
        QUINCENA: "",
        HASH: 0,
        NOMBRE: "",
        "FECHA INGRESO": "",
        "FECHA ANIV.": "",
        "Años cumplidos": "",
        FOLIO: "",
        "INICIAL 2022": 0,
        "DIAS ANIV": 0,
        GOZADOS: 0,
        "Finiquitos / Liq.": "",
        DISPONIBLES: 0,
        "ANIV PAGADOS": 0,
        "ACUM DIAS PEND DE PAGO DE PV": 0,
        unnamed1: "",
        Estatus: "",
        "dias pv": 0,
        "periodo pago pv": "",
        Obs: "",
        unnamed2: "",
        unnamed3: "",
        unnamed4: "",
        unnamed5: "",
        unnamed6: "",
        unnamed7: "",
    }

    const keys = Object.keys(records)

    for (const i in keys)
        if (typeof records[keys[i]] === "number")
            records[keys[i]] = row[i]?.length > 0 ? Number.parseFloat(row[i]) : 0
        else
            records[keys[i]] = row[i]

    return records
})

const employeesNames: Set<String> = new Set(test.map(row => row.NOMBRE))

for (const currentName of employeesNames) {
    const records = test.filter(row => row.NOMBRE === currentName)

    employees = [...employees, {
        id: records[0].HASH,
        name: currentName,
        records: records,
        totalAvailable: 0
    }]
}

for (const employee of employees) {
    employee.totalAvailable = employee.records[0].DISPONIBLES

    for (const record of employee.records.slice(1)) {
        employee.totalAvailable += record["DIAS ANIV"]
        employee.totalAvailable += (record.GOZADOS > 0 ? (-record.GOZADOS) : record.GOZADOS)
    }
}

for (const employee of employees) {
    const records = employee.records.length

    Deno.test(`Testing ${employee.name}'s available vacations`, () => {
        assertEquals(employee.totalAvailable, employee.records[records - 1].DISPONIBLES)
    })
}

let reportString = `#,NOMBRE,DIAS DISPONIBLES\n`

reportString += `${
    employees.slice(0).map(({id, name, totalAvailable}) =>
        `${id},${name},${totalAvailable}`).join("\n")}`

Deno.writeFileSync("./test/rsrc/report.csv", new TextEncoder().encode(reportString))
